BEGIN {
    tag_id="App ID";
    tag_app_type="App Type";
	tag_pkt_sent = "Packet Sent";
	tag_pkt_drop = "Packet Dropped";
	tag_pkt_recv = "Packet Received";
	tag_drop_ratio = "Drop Ratio";
	tag_bytes_sent = "Bytes Sent";
	tag_bytes_recv = "Bytes Received";
	tag_thruput = "Throughput";
	tag_begin = "App Begin";
	tag_end = "App End";
	tag_on = "App On";

	cbr_attr[tag_begin] = 0.0;
	cbr_attr[tag_on] = 0;

	cbr_stats[tag_pkt_sent] = 0;
	cbr_stats[tag_pkt_drop] = 0;
	cbr_stats[tag_pkt_recv] = 0;
	cbr_stats[tag_drop_ratio] = 0.0;
	cbr_stats[tag_bytes_sent] = 0;
	cbr_stats[tag_thruput] = 0.0;

	tcp_stats[tag_pkt_sent] = 0;
	tcp_stats[tag_pkt_drop] = 0;
	tcp_stats[tag_pkt_recv] = 0;	
	tcp_stats[tag_drop_ratio] = 0.0;
	tcp_stats[tag_bytes_sent] = 0;

	tcp_attr[tag_begin] = 0.0;
	tcp_attr[tag_on] = 0;
}

{
	event = $1;
	time = $2;
	from_node = $3;
	to_node = $4;
	pkt_type = $5;
	size = $6;
	fd_source = $9;
	fd_dest = $10
	pk_id = $12;

	line_no ++;

	split(fd_source, src_addr, "."); 
	split(fd_dest, dest_addr, ".");
		
	if (event=="-" && from_node==src_addr[1]) {
		if (pkt_type=="cbr") {
			cbr_stats[tag_pkt_sent]++;
			cbr_attr[tag_on] = 1;
			if (cbr_attr [tag_begin] == 0.0) cbr_attr [tag_begin] = time;
		}
		else if (pkt_type=="tcp") {
			tcp_stats[tag_pkt_sent]++;
			tcp_attr[tag_on] = 1;
			if (tcp_attr [tag_begin] == 0.0) tcp_attr [tag_begin] = time;
		}
	} 

	if (event=="r" && to_node==dest_addr[1] ) {
		if (pkt_type=="cbr") {
            cbr_stats[tag_id]=fd_source"->"fd_dest;
            cbr_stats[tag_app_type]=pkt_type;
			cbr_stats[tag_pkt_recv]++;
			cbr_stats[tag_bytes_recv]+=size;
			cbr_attr [tag_end] = time;
		}
		else if (pkt_type=="tcp") {
            tcp_stats[tag_id]=fd_source"->"fd_dest;
            tcp_stats[tag_app_type]=pkt_type;
			tcp_stats[tag_pkt_recv]++;
			tcp_stats[tag_bytes_recv]+=size;
			tcp_attr [tag_end] = time;
		}
	}
} 

function show_stats(attr, stats) {
	if (attr[tag_on]==1) {
		stats[tag_pkt_drop] = stats[tag_pkt_sent] - stats[tag_pkt_recv] ;
		stats[tag_drop_ratio] = (stats[tag_pkt_drop] / stats[tag_pkt_sent]);
        #print(cbr_attr[tag_begin]);
        #print(cbr_attr[tag_end]);
		duration = cbr_attr [tag_end] - cbr_attr [tag_begin];
		stats[tag_thruput] = 8*stats[tag_bytes_recv] / duration;
        printf(stats[tag_id]"\t"stats[tag_app_type]"\t"stats[tag_thruput]"\t\t\t"stats[tag_pkt_sent]"\t\t"stats[tag_pkt_recv]"\t\t"stats[tag_drop_ratio]*100"\n");
	}
}

END {   
    printf("S->D\t\tType\tThroughput(b/s)\t\tPkts Sent\tPkts Recv\tDrop Ratio\n");
    show_stats(cbr_attr, cbr_stats);
    show_stats(tcp_attr, tcp_stats);
}
