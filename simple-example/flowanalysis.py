#!/usr/bin/python3

"""A module to parse ns2 traces. This parsing is very generic, and
as a result, it is quite slow. However, it provides a useful framework for
extracting specific data from files, without having to fight with the raw file
format. The typical way to use it is to call parseFlowsFromTrace() on a trace
to return a set of Flow objects in that trace. Then use the functions on the
Flow and PacketEvent objects to get the data you want.

If executed directly, it parses the trace file passed as the first parameter
and prints the throughput and drop ratios for all the flows in the file."""

'''
Line 6167: receive Packet [1027]
Line 6171: receive Packet [1028]
Line 6172: receive Packet [1029]
Line 6173: receive Packet [1030]
Line 6174: receive Packet [1031]
Source -> Dest	Throughput(b/s)	Sent	Recv	Drop Ratio
1.0->3.1	0.000000	550	550	0.000000
Source -> Dest	Throughput(b/s)	Sent	Recv	Drop Ratio
0.0->3.0	0.000000	246	246	0.000000
'''

import logging

EVENT_SEND = "-"
EVENT_RECEIVE = "r"
EVENT_DROP = "d"
EVENT_FORWARD = "f"

EVENT_TYPE = {
	EVENT_SEND: "-",
	EVENT_RECEIVE: "r",
	EVENT_DROP: "d",
	EVENT_FORWARD: "f",
	}
	
TRACE_AGENT = "AGT"
TRACE_ROUTER = "RTR"
TRACE_MAC = "MAC"

TAG_EVTYPE = "evtype"
TAG_SOURCE = "-Is"
TAG_DESTINATION = "-Id"
TAG_SIZE = "-Il"
TAG_TIME = "-t"
TAG_UID = "-Ii"
TAG_FLOWID = "-If"
TAG_TRACELEVEL = "-Nl"
TAG_REASON = "-Nw"
TAG_PACKET_TYPE = "-P"

TAG_FNODE = 'from_node'
TAG_TNODE = 'to_node'


IP_HEADER_SIZE = 20


class PacketEvent(object):
	"""Stores information about the beginning and end of each packet, from being sent to being received or dropped."""
	def __init__( self, sentTime, byte_countize, uid ):
		"""sentTime - The time this packet was first sent (created).
		byte_countize - Size of packet in byte_count.
		uid - Used to uniquely identify this packet."""
		
		assert( sentTime >= 0 )
		assert( byte_countize > 0 )
		#assert( uid >= 0 )
		
		self._sentTime = sentTime
		self._size = byte_countize
		self._uid = uid
		
		self._received = 0
		self._finishTime = None
		
		# Stores all drops: maps drop times to reasons
		self._drops = {}
		
	def valid( self ):
		"""A valid packet event is one that has both been sent and
		received or dropped. Returns True or False."""
		
		# Has been received, is valid
		if ( self._received == 1 ):
			assert( self._finishTime != None )
			return 1
	
		assert( self._received == 0 )
		if ( len( self._drops ) > 0 ):
			assert( self._finishTime != None )
			return 1
		
		return 0
	
	def received( self, receivedTime ):
		"""Mark this packet as being received at receivedTime."""
		
		assert( not self._received ) # Must not have been received yet
		assert( receivedTime > self._sentTime )
		
		self._received = 1
		# This will replace the time for any drops
		self._finishTime = receivedTime
		
	def wasReceived( self ):
		"""Returns True if this packet was received at least once."""
		return self._received == 1
	
	def droppedDuplicates( self ):
		"""Returns the number of duplicates that were dropped. It
		returns the total number of drops, if it was received at least
		once. It returns the number of drops minus one if it was not
		received, and it returns 0 if there have been no events.""" 
		
		if ( not self.valid() ):
			return 0
		if ( self._received ):
			return len( self._drops )
		else:
			return len( self._drops ) - 1
	
	def dropped( self, dropTime):
		"""Mark this packet as being dropped at dropTime."""
		assert( dropTime >= self._sentTime )		
		assert( not dropTime in self._drops )
		
		if ( not self._received ):
			#dropTimes = self._drops.keys()
			#dropTimes.sort()
			self._finishTime = sorted(self._drops.items())[0]
	
	def delay( self ):
		"""Return the delay experienced by this packet. Will fail if the
		packet has not been received."""
		assert( self._received )
		return self._finishTime - self._sentTime
	
	def sentTimeCompare( self, other ):
		"""Compares two valid packet events based on sent time."""
		assert( self.valid() and other.valid() )
		
		return cmp( self._sentTime, other._sentTime )

	def finishTimeCompare( self, other ):
		"""Compares two valid packet events based on finish time."""
		assert( self.valid() and other.valid() )
		
		return cmp( self._finishTime, other._finishTime )


class Flow(object):
	"""Stores PacketEvents and performs computations to obtain statistics about the flows."""
	def __init__( self, sourceAddr, sourcePort, destinationAddr, destinationPort ):
		self._packets = {}
		
		assert( not (sourceAddr == destinationAddr and sourcePort == destinationPort ) )
		
		self._sourceAddr = sourceAddr
		self._sourcePort = sourcePort
		self._destinationAddr = destinationAddr
		self._destinationPort = destinationPort
		
		self._startTime = None
		self._finishTime = None

		self._pktCountRecv = 0
		self._pktCountSend = 0

		self._key = sourceAddr+'.'+sourcePort+'->'+destinationAddr+'.'+destinationPort

	def setAppType(self, appType):
		self._appType = appType
        
	def getAppType(self):
		return self._appType

	def getPktCountRecv(self):
		return self._pktCountRecv

	def getPktCountSend(self):
		return self._pktCountSend

	def getKey(self):
		return self._key
	
	def send( self, sentTime, byte_countize, uid ):
		"""Adds a new packet being sent event to this flow."""
		if uid in self._packets: return False

		self._packets[uid] = PacketEvent( sentTime, byte_countize, uid )
		self._pktCountSend+=1
		
		# Update the start/end times, if possible
		# This means that even invalid packet events will be recorded
		if ( self._startTime == None or sentTime < self._startTime ): self._startTime = sentTime
		if ( self._finishTime == None or sentTime > self._finishTime ): self._finishTime = sentTime
		return True
	
	def receive( self, receivedTime, uid ):
		assert( uid in self._packets )

		self._packets[uid].received( receivedTime )
		self._pktCountRecv += 1
		
		# Update the end times, if possible
		# This means that even invalid packet events will be recorded
		if ( self._finishTime == None or receivedTime > self._finishTime ): 
			self._finishTime = receivedTime

		
	def validPackets( self ):
		return [ p for p in self._packets.values() if p.valid() ]
	
	def startTime( self ):
		"""Find the lowest start time (among valid packets)."""
		assert( len( self._packets ) > 0 )
		return self._startTime
	
	def finishTime( self ):
		assert( len( self._packets ) > 0 )
		return self._finishTime
	
	def byte_countReceived( self ):
		total = 0
		
		for p in self._packets.values():
			if ( p._received ):
				total += p._size
		
		return total
	
	def droppedPackets( self ):
		dropCount = 0
		for p in self._packets.values():
			if ( p.wasDropped() ):
				dropCount += 1
		return dropCount
	
	def droppedDuplicates( self ):
		dropCount = 0
		for p in self._packets.values():
			dropCount += p.droppedDuplicates()
		return dropCount
	
	def packetsReceivedInRange( self, startTime, stopTime ):
		"""Returns the packets received in the interval [startTime, stopTime] (inclusive)."""
		return [ p for p in self._packets.values() if
			p.valid() and p.wasReceived() and startTime <= p._finishTime and p._finishTime <= stopTime ]
	
	def throughput( self, averagingInterval = None, discardInitial = None, discardFinal = None ):
		#TODO: debug throughput()  #TODO : how finishTime is determined

		"""Returns the throughput of this flow. Averaging interval
		should be the time you want to average the throughput
		over. By default, it is the entire duration of the flow. 
		"""
		
		assert( len( self._packets ) > 0 )
		
		if averagingInterval == None:
			if discardInitial == None: discardInitial = 0
			if discardFinal == None: discardFinal = 0
			
			start = self.startTime() + discardInitial
			stop = self.finishTime() - discardFinal
			print(start)
			print(stop)
			
			#duration = float( int( stop - start + 0.5 ) )
			duration = stop - start
			#stop = start + duration
			
			if duration <= 0:
				# No duration means no byte_count = zero throughput
				return 0
			
			byte_count = 0
			for p in self.packetsReceivedInRange( start, stop ):
				byte_count += p._size
				
			#covert byte size into bits
			return byte_count*8 / duration
		else:
			assert( averagingInterval > 0 )
			
			if discardInitial == None: discardInitial = self.startTime()
			if discardFinal == None: discardFinal =  self.finishTime()
			
			# Now go find all received packets
			packets = [ p for p in self.validPackets() if p.wasReceived() ]
			packets.sort( PacketEvent.finishTimeCompare )
			
			data = []
			intervalCount = 0
			start = discardInitial
			end = start + averagingInterval
			byte_count = 0
			
			for p in packets:
				# If this packet is after the interval, record the data
				while p._finishTime > end:
					data.append( [ end, float( byte_count ) / averagingInterval ] )
					
					# Use multiplication to avoid cumulative float errors when adding floats
					intervalCount += 1
					start = discardInitial + intervalCount * averagingInterval
					end = discardInitial + (intervalCount+1) * averagingInterval
					byte_count = 0.0
				
				# If the current interval begins where we are supposed to stop, STOP!
				if start >= discardFinal:
					break
				
				# Ignore packets outside the interval
				# The initial packets could start before the interval
				if ( start < p._finishTime ):
					assert( p._finishTime <= end )
					byte_count += p._size
			
			# Go and append all the remaining intervals
			while ( start < discardFinal ):
				data.append( [ end, float( byte_count ) / averagingInterval ] )
				
				# Use multiplication to avoid cumulative float errors when adding floats
				intervalCount += 1
				start = discardInitial + intervalCount * averagingInterval
				end = discardInitial + (intervalCount+1) * averagingInterval
				byte_count = 0.0
			
			return data
		
def parseFlowsFromTrace( lines ):
	"""Parses NS2 new style trace file lines and returns a dictionary of Flows contained in the file."""
	# Flows are uniquely identified by (saddr, sport, daddr, dport)
	# Dictionary maps flow key  to flow objects
	flows = {}

	for i, line in enumerate(lines):
		lineno = i+1
		data = line.split()
		
		# Skip blank lines
		if len( data ) == 0: continue
		
		tags = {}
		tags[TAG_EVTYPE] = data[0]
		tags[TAG_TIME] = float(data[1])
		tags[TAG_FNODE] = data[2]
		tags[TAG_TNODE] = data[3]
		tags[TAG_PACKET_TYPE] = data[4]
		tags[TAG_SIZE] = int(data[5])
		tags[TAG_SOURCE] = data[-4]
		tags[TAG_DESTINATION] = data[-3]
		tags[TAG_UID] = data[-1]
	
		if ( (not tags[TAG_EVTYPE] in EVENT_TYPE) or (tags[TAG_PACKET_TYPE] == 'ack' ) ):
			# Skip lines that we don't understand
			continue

		(saddr, sport) = tags[TAG_SOURCE].split( "." )
		(daddr, dport) = tags[TAG_DESTINATION].split( "." )	
	
		flowKey = tags[TAG_SOURCE]+'->'+tags[TAG_DESTINATION] 
		
		if ( tags[TAG_EVTYPE] == EVENT_SEND  ):
			
			#create a flow
			if ( not flowKey  in flows ): 
				flows[flowKey] = Flow(  saddr ,  sport ,  daddr ,  dport  )
				flows[flowKey].setAppType( tags[TAG_PACKET_TYPE] )
				
			if (saddr==tags[TAG_FNODE] ) :
				flows[flowKey].send( float( tags[TAG_TIME] ), tags[TAG_SIZE] + IP_HEADER_SIZE, tags[TAG_UID] )
				#logging('Line {}: send Packet [{}]'.format( lineno, tags[TAG_UID] ))
			
		elif ( tags[TAG_EVTYPE]  == EVENT_RECEIVE ):
			#~ print line
			#TODO: will the throughput be calucalted by node or by flow?
			#REPLY: at node level, you'll have to make stats per app per source node.
			if ( tags[TAG_TNODE]==daddr ):
				#if (tags[TAG_PACKET_TYPE]=='cbr'):
				#	print('Line {}: receive Packet [{}]'.format( lineno, tags[TAG_UID] ))
				#print(flowKey);
				flows[flowKey].receive( ( tags[TAG_TIME] ), tags[TAG_UID] )
		elif ( tags[TAG_EVTYPE]  == EVENT_DROP ):
			#TODO: is there explicit drop event in classic trace?
			#Reply: No.
			#flows[flowKey].drop(  tags[TAG_TIME] ,tags[TAG_UID])
			pass
	
	return flows.values()

if __name__ == '__main__':
	import sys
    
	if len( sys.argv ) != 2:
		print("count-throughput.py <trace file>")
		print()
		print("\tThis will automatically locate all flows in the trace file")
		print("\tand report the following statistics:")
		print()
		print("\tthroughput\tdrop ratio")
		sys.exit( 1 )
		
	with open( sys.argv[1] , 'r') as trace:
	    flows = parseFlowsFromTrace( trace )
	
	
	for flow in flows:
		pktCountSent = flow.getPktCountSend()
		pktCountRecv = flow.getPktCountRecv()
		pktCountDrop = pktCountSent - pktCountRecv
		dropRatio = float( pktCountDrop ) / pktCountSent * 100
		print("S->D\t\ttype\tThroughput(b/s)\tSent\tRecv\tDrop Ratio")
		print("%s\t%s\t%d\t\t%d\t%d\t%.2f%%" % ( flow.getKey(), flow.getAppType(), flow.throughput(), \
			pktCountSent, pktCountRecv, dropRatio))
